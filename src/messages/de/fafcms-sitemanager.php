<?php
return [
    'The SEO description should be about 160 characters long.' => 'Die SEO-Beschreibung sollte ca. 160 Zeichen lang sein.',
    'Site' => 'Seite',
    'Sites' => 'Seiten',
    'Page properties' => 'Seiteneigenschaften',
    'Parent container' => 'Übergeordneter Container',
    'Parent site' => 'Übergeordnete Seite',
    'None' => 'Keine',
    'Topics' => 'Themen',
    'Topic' => 'Thema',
    'Snippets' => 'Schnipsel',
    'Snippet' => 'Schnipsel',
    'Website' => 'Webseite',
    'Menus' => 'Menüs',
];
