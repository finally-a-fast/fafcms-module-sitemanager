<?php

namespace fafcms\sitemanager\updates\migrations;

use fafcms\sitemanager\models\Contentmeta;
use fafcms\updater\base\Migration;

/**
 * Class m201023_133929_sitemap_priority
 *
 * @package fafcms\sitemanager\updates\migrations
 */
class m201023_133929_sitemap_priority extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->alterColumn(Contentmeta::tableName(), 'sitemap_priority', $this->tinyInteger(2)->unsigned()->notNull()->defaultValue(5));

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->alterColumn(Contentmeta::tableName(), 'sitemap_priority', $this->tinyInteger(1)->notNull()->defaultValue(5));

        return true;
    }
}
