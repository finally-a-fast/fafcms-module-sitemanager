<?php

namespace fafcms\sitemanager\updates\migrations;

use fafcms\sitemanager\models\Site;
use fafcms\updater\base\Migration;

/**
 * Class m201209_102924_parent_site
 *
 * @package fafcms\sitemanager\updates\migrations
 */
class m201209_102924_parent_site extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->dropForeignKey('fk-site-parent_site_id', Site::tableName());
        $this->dropIndex('idx-site-parent_site_id', Site::tableName());
        $this->dropColumn(Site::tableName(), 'parent_site_id');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->addColumn(Site::tableName(),'parent_site_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('display_end'));
        $this->createIndex('idx-site-parent_site_id', Site::tableName(), ['parent_site_id'], false);
        $this->addForeignKey('fk-site-parent_site_id', Site::tableName(), 'parent_site_id', Site::tableName(), 'id', 'SET NULL', 'CASCADE');

        return true;
    }
}
