<?php

namespace fafcms\sitemanager\updates;

use fafcms\sitemanager\updates\migrations\m201209_102924_parent_site;
use fafcms\updater\base\Update;

/**
 * Class u201209_102924_parent_site
 *
 * @package fafcms\sitemanager\updates
 */
class u201209_102924_parent_site extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        return $this->migrateUp(m201209_102924_parent_site::class);
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        return $this->migrateDown(m201209_102924_parent_site::class);
    }
}
