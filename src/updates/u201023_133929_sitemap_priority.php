<?php

namespace fafcms\sitemanager\updates;

use fafcms\sitemanager\updates\migrations\m201023_133929_sitemap_priority;
use fafcms\updater\base\Update;

/**
 * Class u201023_133929_sitemap_priority
 *
 * @package fafcms\sitemanager\updates
 */
class u201023_133929_sitemap_priority extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        return $this->migrateUp(m201023_133929_sitemap_priority::class);
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        return $this->migrateDown(m201023_133929_sitemap_priority::class);
    }
}
