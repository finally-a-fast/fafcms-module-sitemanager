<?php

namespace fafcms\sitemanager\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\sitemanager\abstracts\models\BaseLayout;
use fafcms\sitemanager\Bootstrap;

use fafcms\fafcms\{inputs\AceEditor, items\Card, items\Column, items\FormField, items\Row, items\Tab, minifier\Minifier, minifier\MinifierSrcContent};

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%layout}}".
 *
 * @package fafcms\sitemanager\models
 */
class Layout extends BaseLayout
{
    use MultilingualTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/layout';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'view-compact-outline';
    }
    //endregion BeautifulModelTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        $fieldConfig = parent::getFieldConfig();
        $fieldConfig['content'] = [
            'type' => AceEditor::class,
        ];

        return $fieldConfig;
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Access restrictions',
                                                ],
                                                'icon' => 'file-eye-outline',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-display_start' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                'field-display_end' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Content'],
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                'field-content' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'content',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        $indexView = parent::indexView();
        unset($indexView['default']['content']);

        return $indexView;
    }
    //endregion IndexViewInterface implementation

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentmetas() //is this needed?
    {
        return $this->hasMany(Contentmeta::className(), ['layout_id' => 'id']);
    }

    public static function getContentById($id)
    {
        $cacheName = 'fafcms-sitemanger-layout-' . $id;
        $content = Yii::$app->cache->get($cacheName);

        if (!Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('enable_snippet_cache') || $content === false) {
            $snippet = self::find()->select('content')->where(['id' => $id])->asArray()->one();

            if ($snippet === null) {
                return null;
            }

            $content = $snippet['content'];

            $content = (new Minifier([
                'src' => [
                    new MinifierSrcContent([
                        'type' => 'html',
                        'content' => $content
                    ])
                ]
            ]))->run();

            Yii::$app->cache->set($cacheName, $content, null);
        }

        return $content;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Yii::$app->cache->delete('fafcms-sitemanger-layout-' . $this->id);
    }
}
