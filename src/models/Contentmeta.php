<?php

namespace fafcms\sitemanager\models;

use fafcms\fafcms\inputs\DropDownList;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\classes\OptionProvider;
use fafcms\helpers\traits\MultilingualTrait;
use fafcms\sitemanager\Bootstrap;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use fafcms\sitemanager\abstracts\models\BaseContentmeta;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use Closure;

/**
 * This is the model class for table "{{%contentmeta}}".
 *
 * @package fafcms\sitemanager\models
 */
class Contentmeta extends BaseContentmeta
{
    use MultilingualTrait;

    public const CACHE_CONTENTMETA_TREE = 'fafcms-sitemanger-contentmeta-tree';

    /**
     * @return string[]
     */
    public function autoTranslateRelations(): array
    {
        return [
            'parentContentmeta'
        ];
    }

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/contentmeta';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'tab-plus';
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        $contentIcon = 'help-rhombus-outline';
        $contentIconLabel = $model['model_class'];

        if (empty($model['model_class'])) {
            $contentIconLabel = Yii::t('fafcms-core', 'Container');
            $contentIcon = 'folder-network-outline';
        } elseif (Yii::$app->fafcms->isValidContentClass($model['model_class'])) {
            $contentIconLabel = $model['model_class']::instance()->getEditDataSingular();
            $contentIcon = $model['model_class']::instance()->getEditDataIcon();
        }

        if ($model['title'] !== null && $model['title'] !== '') {
            $contentTitle = $model['title'];
        } else {
            $contentTitle = ($html?'<i>':'').$contentIconLabel.($html?'</i>':'');
        }

        if ($html) {
            $contentTitle = Html::tag('i', '', ['title' => $contentIconLabel, 'class' => 'mdi mdi-' . $contentIcon]).' '.$contentTitle;
        }

        return $contentTitle;
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.title',
                static::tableName() . '.model_class'
            ])
            ->setSort([static::tableName() . '.title' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        $disabledItems = [];

        if (($this->contentmeta->id ?? null) !== null) {
            $disabledItems[$this->contentmeta->id] = ['disabled' => true];
        }

        return array_merge(parent::getFieldConfig(), [
            'parent_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('parent_contentmeta_id', false),
                'relationClassName' => static::class,
                'options' => ['options' => ['options' => $disabledItems]]
            ],
            'topicids' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('topicids', false),
                'options' => [
                    'options' => ['multiple' => true]
                ]
            ],
            'og_determiner' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('og_determiner', false),
            ],
            'og_type' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('og_type', false),
            ],
            'sitemap_changefreq' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('sitemap_changefreq', false),
            ],
        ]);
    }
    //endregion FieldConfigInterface implementation

    /**
     * @param string $url
     * @param bool   $partial
     * @param bool   $ignoreType
     * @param bool   $withInactive
     *
     * @return array|mixed|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function getIdByUrl(string $url, bool $partial = false, bool $ignoreType = false, bool $withInactive = false)
    {
        $cacheName = Yii::$app->fafcms->getCacheName('contentmeta_mapping' . ($partial ? '_partial' : '') . '_T' . ($ignoreType ? 'TRUE' : 'FALSE') . ($withInactive ? '_with_inative' : ''));
        $id = null;

        if ($url !== null) {
            $contentmetaMapping = Yii::$app->cache->get($cacheName);
            $id = $contentmetaMapping[$url] ?? null;

            if ($partial || $id === null) {
                $urlParts = explode('/', $url);
                $lastContentmeta = null;
                $lastUrlPartIndex = count($urlParts) - 1;

                foreach ($urlParts as $index => $urlPart) {
                    $parentContentmetaId = ($lastContentmeta !== null ? $lastContentmeta['id'] : null);
                    $action = ($index === $lastUrlPartIndex && !$ignoreType ? 'content' : 'one');

                    $contentmeta = (Yii::$app->dataCache->data['contentmetaIdByUrlPart'][$urlPart][$parentContentmetaId][$withInactive][$action] ?? null);

                    if ($contentmeta === null) {
                        $contentmeta = self::find()
                            ->select('id, slug, parent_contentmeta_id')
                            ->andWhere([
                                'slug' => $urlPart,
                                'parent_contentmeta_id' => $parentContentmetaId
                            ])
                            ->asArray();

                        if ($withInactive) {
                            $contentmeta = $contentmeta->byStatus('all', false, false);
                        }

                        $contentmeta = $contentmeta->$action();
                        Yii::$app->dataCache->data['contentmetaIdByUrlPart'][$urlPart][$parentContentmetaId][$withInactive][$action] = $contentmeta;
                    }

                    if ($contentmeta === null) {
                        if ($partial) {
                            $remainingUrl = array_splice($urlParts, $index);

                            return [
                                'remainingUrl' => $remainingUrl,
                                'siteUrl' => $urlParts,
                                'site' => $lastContentmeta,
                            ];
                        }

                        // TODO
                        //var_dump($urlPart, ($lastContentmeta !== null ? $lastContentmeta['id'] : null));die();
                        return null;
                    }

                    $lastContentmeta = $contentmeta;
                }

                if (!$partial && $lastContentmeta !== null) {
                    $id = $lastContentmeta['id'];
                    $contentmetaMapping = Yii::$app->cache->get($cacheName);
                    $contentmetaMapping[$url] = $id;
                    Yii::$app->cache->set($cacheName, $contentmetaMapping);
                }
            }
        }

        return $id;
    }

    /**
     * @param      $id
     * @param bool $withInactive
     * @param int  $deep
     *
     * @return mixed|string|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function getUrlById($id, bool $withInactive = false, int $deep = 0)
    {
        if ($id !== null) {
            $cacheName = Yii::$app->fafcms->getCacheName('contentmeta_mapping' . ($withInactive ? '_with_inative' : ''));
            $contentmetaMapping = Yii::$app->cache->get($cacheName);

            if (!$contentmetaMapping) {
                $contentmetaMapping = [];
            } else {
                $contentmetaMapping = array_flip($contentmetaMapping);
            }

            if (!isset($contentmetaMapping[$id])) {
                $contentmeta = self::find()->select('slug, parent_contentmeta_id')->where(['id' => $id])->asArray();

                if ($withInactive) {
                    $contentmeta->byStatus('all', false, false);
                }

                $contentmeta = $contentmeta->one();

                if ($contentmeta === null) {
                    return null;
                }

                $url = $contentmeta['slug'];

                if ($contentmeta['parent_contentmeta_id'] !== null) {
                    if ($deep >= 150) {
                        throw new \Exception('Contentmeta loop detected: ' . print_r($contentmeta, true));
                    }

                    $url = self::getUrlById($contentmeta['parent_contentmeta_id'], $withInactive, $deep + 1) . '/' . $url;
                }

                $contentmetaMapping[$id] = $url;

                Yii::$app->cache->set($cacheName, array_flip($contentmetaMapping));
            }

            return $contentmetaMapping[$id];
        }

        return null;
    }

    public static function getEditUrlById($id) {
        $contentmeta = self::find()->andWhere(['id' => $id])->content();

        if ($contentmeta !== null && $contentmeta['model_class'] !== null && Yii::$app->fafcms->isValidContentClass($contentmeta['model_class'])) {
            return $contentmeta['model_class']::instance()->getEditData()['url'];
        }

        return null;
    }

    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [[
            'class' => SluggableBehavior::class,
            'attribute' => ['title'],
            'ensureUnique' => true,
            'uniqueValidator' => ['targetAttribute' => ['project_id', 'projectlanguage_id', 'parent_contentmeta_id', 'slug']],
            'immutable' => true
        ]]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        $rules = parent::rules();
        unset($rules['exist-layout_id'], $rules['exist-parent_contentmeta_id']);

        return array_merge($rules, [
            'integer-sitemap_priority' => ['sitemap_priority', 'integer', 'min' => 0, 'max' => 10],
            'each-topicids' => ['topicids', 'each', 'rule' => ['in', 'range' => Topic::find()->select('id')->column()]],
            'unique-slug-parent_contentmeta_id' => [['project_id', 'projectlanguage_id', 'parent_contentmeta_id', 'slug'], 'unique', 'targetAttribute' => ['project_id', 'projectlanguage_id', 'slug', 'parent_contentmeta_id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'Topicids' => Topic::instance()->getEditDataPlural(),
        ]);
    }

    /**
     * @return array
     */
    public function attributeOptions(): array
    {
        return array_merge(parent::attributeOptions(), [
            'topicids' => static function($properties = []) {
                return Topic::getOptionProvider($properties)->setEmptyItem(false)->getOptions();
            },
            'og_determiner' => [
                '' => Yii::t('fafcms-sitemanager', 'OG_DETERMINER_BLANK'),
                'a' => Yii::t('fafcms-sitemanager', 'OG_DETERMINER_A'),
                'an' => Yii::t('fafcms-sitemanager', 'OG_DETERMINER_AN'),
                'the' => Yii::t('fafcms-sitemanager', 'OG_DETERMINER_THE'),
                'auto' => Yii::t('fafcms-sitemanager', 'OG_DETERMINER_AUTO'),
            ],
            'og_type' => [
                'website' => Yii::t('fafcms-sitemanager', 'OG_TYPE_WEBSITE'),
                'music.song' => Yii::t('fafcms-sitemanager', 'OG_TYPE_MUSIC_SONG'),
                'music.album' => Yii::t('fafcms-sitemanager', 'OG_TYPE_MUSIC_ALBUM'),
                'music.playlist' => Yii::t('fafcms-sitemanager', 'OG_TYPE_MUSIC_PLAYLIST'),
                'music.radio_station' => Yii::t('fafcms-sitemanager', 'OG_TYPE_MUSIC_RADIO_STATION'),
                'video.movie' => Yii::t('fafcms-sitemanager', 'OG_TYPE_VIDEO_MOVIE'),
                'video.episode' => Yii::t('fafcms-sitemanager', 'OG_TYPE_VIDEO_EPISODE'),
                'video.tv_show' => Yii::t('fafcms-sitemanager', 'OG_TYPE_VIDEO_TV_SHOW'),
                'video.other' => Yii::t('fafcms-sitemanager', 'OG_TYPE_VIDEO_OTHER'),
                'article' => Yii::t('fafcms-sitemanager', 'OG_TYPE_ARTICLE'),
                'book' => Yii::t('fafcms-sitemanager', 'OG_TYPE_BOOK'),
                'profile' => Yii::t('fafcms-sitemanager', 'OG_TYPE_PROFILE'),
            ],
            'sitemap_changefreq' => [
                'always' => Yii::t('fafcms-sitemanager', 'Always'),
                'hourly' => Yii::t('fafcms-sitemanager', 'Hourly'),
                'daily' => Yii::t('fafcms-sitemanager', 'Daily'),
                'weekly' => Yii::t('fafcms-sitemanager', 'Weekly'),
                'monthly' => Yii::t('fafcms-sitemanager', 'Monthly'),
                'yearly' => Yii::t('fafcms-sitemanager', 'Yearly'),
                'never' => Yii::t('fafcms-sitemanager', 'Never'),
            ]
        ]);
    }

    /**
     * @return array
     */
    public function getContentmetaItems(): array
    {
        return Yii::$app->cache->getOrSet(self::CACHE_CONTENTMETA_TREE, function() {
            /**
             * @var $currentProjectLanguageId Projectlanguage
             */
            $currentProjectLanguageId = Yii::$app->fafcms->getCurrentProject()->getProjectProjectlanguages()->where([
                'id' => Yii::$app->fafcms->getCurrentProjectLanguageId()
            ])->one();

            $contentmetaItems = ['' => Html::tag('i', '', ['title' => Yii::t('fafcms-core', 'Website'), 'class' => 'mdi mdi-web']) . ' ' . $currentProjectLanguageId->domain->domain];
            $options = [];

            $this->getContentmetaOptions($contentmetaItems, $options);
            return $contentmetaItems;
        }, 0);
    }

    private $_contentmetaOptionsCache;

    /**
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     */
    private function contentmetaOptionsCache(): ?array
    {
        if ($this->_contentmetaOptionsCache === null) {
            $items = self::find()->asArray()->all();

            if ($items === null) {
                return null;
            }

            $this->_contentmetaOptionsCache = ArrayHelper::index($items, null, function($model) {
                return $model['parent_contentmeta_id']??'';
            });
        }

        return $this->_contentmetaOptionsCache;
    }

    /**
     * @param array    $items
     * @param array    $options
     * @param int|null $parentContentmetaId
     * @param int      $level
     *
     * @throws \yii\base\InvalidConfigException
     */
    private function getContentmetaOptions(array &$items, array &$options, int $parentContentmetaId = null, int $level = 1): void
    {
        foreach ($this->contentmetaOptionsCache()[$parentContentmetaId ?? ''] ?? [] as $option) {
            $levelIcons = '';

            for ($i = 0; $i < $level; $i++) {
                $levelIcons .= '<i class="mdi mdi-subdirectory-arrow-right tiny"></i>';
            }

            $items[$option['id']] = $levelIcons . ' ' . self::extendedLabel($option);

            $this->getContentmetaOptions($items, $options, $option['id'], $level + 1);
        }
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        $changedUrl = $this->isAttributeChanged('slug') || $this->isAttributeChanged('parent_contentmeta_id');

        $result = parent::save($runValidation, $attributeNames);

        if ($changedUrl) {
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_TTRUE'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_TFALSE'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_partial'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_partial_TTRUE'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_partial_TFALSE'));

            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_with_inative'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_TTRUE_with_inative'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_TFALSE_with_inative'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_partial_with_inative'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_partial_TTRUE_with_inative'));
            Yii::$app->cache->delete(Yii::$app->fafcms->getCacheName('contentmeta_mapping_partial_TFALSE_with_inative'));
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @todo check if it is used somewhere. If not remove it AND remove it from ActiveRecord translation function
     * @deprecated
     */
    public function getContentmetas(): \yii\db\ActiveQuery
    {
        return $this->hasMany(self::class, ['parent_contentmeta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @todo check if it is used somewhere. If not remove it
     * @deprecated
     */
    public function getContentmetaTopics(): \yii\db\ActiveQuery
    {
        return $this->hasMany(ContentmetaTopic::class, ['contentmeta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @todo check if it is used somewhere. If not remove it
     * @deprecated
     */
    public function getMenuitems(): \yii\db\ActiveQuery
    {
        return $this->hasMany(Menuitem::class, ['contentmeta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     * @todo check if it is used somewhere. If not remove it
     * @deprecated
     */
    public function getTopics(): \yii\db\ActiveQuery
    {
        return $this->hasMany(Topic::class, ['id' => 'topic_id'])->viaTable(ContentmetaTopic::tableName(), ['contentmeta_id' => 'id']);
    }

    private $_topicids;

    /**
     * @return mixed|null
     */
    public function getTopicids()
    {
        if ($this->_topicids === null) {
            $this->_topicids = $this->savedTopicids;
        }

        return $this->_topicids;
    }

    /**
     * @param $topicSelection
     */
    public function setTopicids($topicSelection): void
    {
        $this->_topicids = $topicSelection;
    }

    public function getSavedTopicids(): array
    {
        return ArrayHelper::getColumn($this->topics, 'id');
    }

    public function afterSave($insert, $changedAttributes): void
    {
        $topicids = $this->topicids;

        if (!is_array($topicids)) {
            $topicids = [];
        }

        $newIds = array_diff($topicids, $this->savedTopicids);
        $removedIds = array_diff($this->savedTopicids, $topicids);

        foreach ($newIds as $newId) {
            $contentmetaTopic = new ContentmetaTopic();
            $contentmetaTopic->topic_id = $newId;
            $contentmetaTopic->contentmeta_id = $this->id;
            $contentmetaTopic->save();
        }

        if (count($removedIds) > 0) {
            ContentmetaTopic::deleteAll(['contentmeta_id' => $this->id, 'topic_id' => $removedIds]);
        }

        Yii::$app->cache->delete(self::CACHE_CONTENTMETA_TREE);
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param array|ActiveRecord $model
     * @param bool               $scheme
     *
     * @return string
     */
    public static function getUrl($model, bool $scheme = false): string
    {
        //todo get domain and protocol http(s) of project of current item
        if ($scheme === true) {
            if (Yii::$app->fafcms->getCurrentProjectLanguage()->domain->force_https || Yii::$app->fafcms->getCurrentProjectLanguage()->domain->is_https_validated) {
                $scheme = 'https';
            } else {
                $scheme = 'http';
            }
        }

        return Url::to(($scheme !== false ? '//' . Yii::$app->fafcms->getCurrentProjectLanguage()->domain->domain : '') . '/' . self::getUrlById($model['id'] ?? null), $scheme);
    }

    /**
     * @return string
     */
    public function getRelativeUrl(): string
    {
        return self::getUrl($this);
    }

    /**
     * @return string
     */
    public function getAbsoluteUrl(): string
    {
        return self::getUrl($this, true);
    }

    /**
     * @return string
     */
    public function getFullSlug(): string
    {
        return str_replace('/', '-', ltrim(self::getUrlById($this['id']), '/'));
    }

    /**
     * @return ActiveRecord|array|null
     */
    public function getContentModel(bool $asArray = false, ?Closure $action = null)
    {
        return self::getContentModelByContentmeta($this, $asArray, $action);
    }

    /**
     * @param int $id
     *
     * @return ActiveRecord|array|null
     */
    public static function getContentModelById(int $id, bool $asArray = false, string $actionCacheName = 'one', ?Closure $action = null)
    {
        if ((Yii::$app->dataCache->data['contentModelById'][$id][$asArray][$actionCacheName] ?? null) === null) {
            $contentmeta = self::find()->where(['id' => $id])->asArray()->one();
            Yii::$app->dataCache->data['contentModelById'][$id][$asArray][$actionCacheName] = self::getContentModelByContentmeta($contentmeta, $asArray, $action);
        }

        return Yii::$app->dataCache->data['contentModelById'][$id][$asArray][$actionCacheName];
    }

    /**
     * @param $contentmeta
     *
     * @return ActiveRecord|array|null
     */
    public static function getContentModelByContentmeta($contentmeta, bool $asArray = false, ?Closure $action = null)
    {
        $contentOptions = Yii::$app->fafcms->getContentClass($contentmeta['model_class']);

        $query = $contentmeta['model_class']::find()
            ->where([$contentOptions['id'] => $contentmeta['model_id']])
            ->asArray($asArray);

        if ($action === null) {
            return $query->one();
        }

        return $action($query);
    }
}
