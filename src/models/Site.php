<?php

namespace fafcms\sitemanager\models;

use fafcms\settingmanager\Module as SettingmanagerModule;
use fafcms\sitemanager\abstracts\models\BaseSite;
use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\ExtendedDropDownList,
    inputs\TinyMce,
    inputs\TextInput,
    inputs\AceEditor,
    minifier\Minifier,
    minifier\MinifierSrcContent
};

use fafcms\helpers\{ActiveRecord,
    classes\OptionProvider,
    interfaces\ContentmetaInterface,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\MultilingualTrait,
    traits\OptionProviderTrait,
    traits\TagTrait,
    traits\OptionTrait};

use fafcms\sitemanager\traits\ContentmetaTrait;
use fafcms\sitemanager\Bootstrap;

use Yii;
use yii\data\ActiveDataProvider;
use yii\validators\DateValidator;

/**
 * This is the model class for table "{{%site}}".
 *
 * @package fafcms\sitemanager\models
 */
class Site extends BaseSite implements ContentmetaInterface
{
    use ContentmetaTrait;
    use TagTrait;
    use MultilingualTrait;
    use AttributeOptionTrait;
    //use HashIdPrimaryKeyTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/site';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'tab-plus';
    }
    //endregion BeautifulModelTrait implementation

    //region ContentmetaInterface implementation
    /**
     * @return string
     */
    public static function contentmetaName(): string
    {
        return 'Site';
    }

    /**
     * @return string
     */
    public static function contentmetaId(): string
    {
        return 'id';
    }

    /**
     * @return string
     */
    public static function contentmetaSiteId(): string
    {
        return 'id';
    }
    //endregion ContentmetaInterface implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        $fieldConfig = parent::getFieldConfig();
        $fieldConfig['content'] = [
            'type' => SettingmanagerModule::getLoadedModule()->getSettingValue('', 'enable_tiny_mce', 0, 0, '', false) ? TinyMce::class : AceEditor::class,
        ];

        return $fieldConfig;
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => \fafcms\fafcms\items\Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Master data'),
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Access restrictions'),
                                                'icon' => 'timetable',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ],
                        ],
                        'row-2' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'contents' => [
                                        'content-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Content'),
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'content',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        $indexView = parent::indexView();
        unset($indexView['default']['content']);

        return $indexView;
    }
    //endregion IndexViewInterface implementation

    public static function getContentById($id)
    {
        $cacheName = 'fafcms-sitemanger-site-' . $id;
        $content = Yii::$app->cache->get($cacheName);

        if (!Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('enable_snippet_cache') || $content === false) {
            $snippet = self::find()->select('content')->where(['id' => $id])->asArray()->one();

            if ($snippet === null) {
                return null;
            }

            $content = $snippet['content'];

            $content = (new Minifier([
                'src' => [
                    new MinifierSrcContent([
                        'type' => 'html',
                        'content' => $content
                    ])
                ]
            ]))->run();

            Yii::$app->cache->set($cacheName, $content, null);
        }

        return $content;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Yii::$app->cache->delete('fafcms-sitemanger-site-' . $this->id);
    }
}
