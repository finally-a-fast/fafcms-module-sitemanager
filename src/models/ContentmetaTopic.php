<?php

namespace fafcms\sitemanager\models;

use fafcms\sitemanager\abstracts\models\BaseContentmetaTopic;

/**
 * This is the model class for table "{{%contentmeta_topic}}".
 *
 * @package fafcms\sitemanager\models
 */
class ContentmetaTopic extends BaseContentmetaTopic
{

}
