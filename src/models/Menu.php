<?php
namespace fafcms\sitemanager\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\sitemanager\abstracts\models\BaseMenu;
use fafcms\fafcms\{
    items\Card,
    items\Column,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\sitemanager\Bootstrap;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%menu}}".
 *
 * @package fafcms\sitemanager\models
 */
class Menu extends BaseMenu
{
    use MultilingualTrait;

    /**
     * @return string[]
     */
    public function autoTranslateRelations(): array
    {
        return [
            'menuitems'
        ];
    }

    //region BeautifulModelTrait implementation
    /**
     * {@inheritDoc}
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/menu';
    }
    //endregion BeautifulModelTrait implementation

    //region EditViewInterface implementation
    /**
     * {@inheritDoc}
     */
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Access restrictions',
                                                ],
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-display_start' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                'field-display_end' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * @return ActiveQuery
     * @todo remove me
     * @deprecated use getMenuMenuitems instad
     */
    public function getMenuitems(): ActiveQuery
    {
        return $this->getMenuMenuitems();
    }
}
