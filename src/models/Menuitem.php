<?php

namespace fafcms\sitemanager\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\fafcms\{
    inputs\TextInput,
    items\Card,
    items\Column,
    items\FormField,
    items\Row,
    items\Tab};
use fafcms\sitemanager\Bootstrap;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use fafcms\sitemanager\abstracts\models\BaseMenuitem;

/**
 * This is the model class for table "{{%menuitem}}".
 *
 * @package fafcms\sitemanager\models
 */
class Menuitem extends BaseMenuitem
{
    use MultilingualTrait;

    //region BeautifulModelTrait implementation
    /**
     * {@inheritDoc}
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/menuitem';
    }

    /**
     * {@inheritDoc}
     */
    public static function editDataIcon($model): string
    {
        return 'note-text';
    }
    //endregion BeautifulModelTrait implementation

    //region FieldConfigInterface implementation
    /**
     * {@inheritDoc}
     */
    public function getFieldConfig(): array
    {
        $fieldConfig = parent::getFieldConfig();
        $fieldConfig['attribute_media'] = [
            'type' => TextInput::class,
        ];

        return $fieldConfig;
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    /**
     * @return array|array[][]
     */
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'type',
                                                    ],
                                                ],
                                                'field-position' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'position',
                                                    ],
                                                ],
                                                'field-menu_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'menu_id',
                                                    ],
                                                ],
                                                'field-parent_menuitem_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'parent_menuitem_id',
                                                    ],
                                                ],
                                                'field-contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'contentmeta_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Access restrictions'
                                                ],
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-display_start' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                'field-display_end' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Attributes'
                                                ],
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                'field-attribute_download' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_download',
                                                    ],
                                                ],
                                                'field-attribute_href' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_href',
                                                    ],
                                                ],
                                                'field-attribute_hreflang' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_hreflang',
                                                    ],
                                                ],
                                                'field-attribute_media' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_media',
                                                    ],
                                                ],
                                                'field-attribute_ping' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_ping',
                                                    ],
                                                ],
                                                'field-attribute_rel' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_rel',
                                                    ],
                                                ],
                                                'field-attribute_target' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_target',
                                                    ],
                                                ],
                                                'field-attribute_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_type',
                                                    ],
                                                ],
                                                'field-attribute_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_class',
                                                    ],
                                                ],
                                                'field-attribute_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_id',
                                                    ],
                                                ],
                                                'field-attribute_style' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_style',
                                                    ],
                                                ],
                                                'field-attribute_tabindex' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_tabindex',
                                                    ],
                                                ],
                                                'field-attribute_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_title',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritDoc}
     */
    public function rules(): array
    {
        $rules = parent::rules();
        unset($rules['required-name']);

        return $rules;
    }

    /**
     * @return ActiveQuery
     * @todo remove me
     * @deprecated use getParentMenuitemMenuitems instead
     */
    public function getMenuitems(): ActiveQuery
    {
        return $this->getParentMenuitemMenuitems();
    }
}
