<?php

namespace fafcms\sitemanager\models;

use fafcms\fafcms\inputs\TinyMce;
use fafcms\fafcms\inputs\AceEditor;
use fafcms\helpers\classes\OptionProvider;
use fafcms\helpers\interfaces\ContentmetaInterface;
use fafcms\helpers\traits\MultilingualTrait;
use fafcms\sitemanager\abstracts\models\BaseTopic;
use fafcms\sitemanager\Bootstrap;
use Yii;
use fafcms\sitemanager\traits\ContentmetaTrait;
use fafcms\settingmanager\Bootstrap as SettingmanagerBootstrap;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%topic}}".
 *
 * @package fafcms\sitemanager\models
 */
class Topic extends BaseTopic implements ContentmetaInterface
{
    use ContentmetaTrait;
    use MultilingualTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/topic';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'folder-star-outline';
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        $label = trim(($model['name'] ?? ''));

        if (($model['parent_topic_id'] ?? null) !== null) {
            $parentItem = self::find()->where(['id' => $model['parent_topic_id']])->asArray()->one();

            if ($parentItem !== null) {
                $label = self::extendedLabel($parentItem) . ' <i class="mdi mdi-arrow-right"></i> ' . $label;
            }
        }

        return $label;
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name',
                static::tableName() . '.parent_topic_id',
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region ContentmetaInterface implementation
    public static function contentmetaName(): string
    {
        return 'Topic';
    }

    public static function contentmetaId(): string
    {
        return 'id';
    }

    public static function contentmetaSiteId(): string
    {
        return 'site_id';
    }
    //endregion ContentmetaInterface implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        $fieldConfig = parent::getFieldConfig();
        $fieldConfig['content'] = [
            'type' => Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('enable_tiny_mce', false)?TinyMce::class:AceEditor::class,
        ];

        return $fieldConfig;
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => \fafcms\fafcms\items\Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Master data'),
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'site_id',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Access restrictions'),
                                                'icon' => 'timetable',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ],
                        ],
                        'row-2' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'contents' => [
                                        'content-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Content'),
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'content',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * @param Topic $topic
     * @return Topic|null
     */
    public static function rootTopic(Topic $topic): ?Topic
    {
        if ($topic->parent_topic_id === null) {
            return $topic;
        }

        $parentTopic = Topic::find()->where(['id' => $topic->parent_topic_id])->one();

        if ($parentTopic !== null) {
            return $parentTopic->getRootTopic();
        }

        return null;
    }

    /**
     * @return Topic|null
     */
    public function getRootTopic(): ?Topic
    {
        return self::rootTopic($this);
    }

    /**
     * @param Topic $topic
     * @return array|null
     */
    public static function childTopicIds(Topic $topic, &$childTopicIds = null): ?array
    {
        $isRoot = false;

        if ($childTopicIds === null) {
            $isRoot = true;
            $childTopicIds = [];
        }

        $childTopics = Topic::find()->select('id, parent_topic_id')->where(['parent_topic_id' => $topic->id])->all();

        foreach ($childTopics as $childTopic) {
            $childTopicIds[$childTopic->id] = true;

            self::childTopicIds($childTopic, $childTopicIds);
        }

        return ($isRoot?array_keys($childTopicIds):$childTopicIds);
    }

    /**
     * @return array|null
     */
    public function getChildTopicIds(): ?array
    {
        return self::childTopicIds($this);
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['status', 'content'], 'string'],
            [['display_start', 'display_end', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['parent_topic_id', 'site_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['site_id', 'name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['parent_topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Topic::className(), 'targetAttribute' => ['parent_topic_id' => 'id']],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(Topic::className(), ['parent_topic_id' => 'id']);
    }
}
