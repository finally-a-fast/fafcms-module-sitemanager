<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-sitemanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-sitemanager
 * @see https://www.finally-a-fast.com/packages/fafcms-sitemanager/docs Documentation of fafcms-sitemanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\sitemanager\traits;

use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\ActiveRecord;
use fafcms\sitemanager\models\Topic;
use fafcms\sitemanager\models\Contentmeta;
use Yii;
use Closure;
use yii\db\ActiveQuery;

/**
 * Trait ContentmetaTrait
 *
 * @package fafcms\sitemanager\traits
 */
trait ContentmetaTrait
{
    /**
     * @return string[]
     */
    public function autoTranslateRelationsContentmetaTrait(): array
    {
        return [
            'contentmeta'
        ];
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getDefaultEditViewItemsContentmetaTrait(): array
    {
        return [
            'tab-1' => [
                'contents' => [
                    'row-1' => [
                        'contents' => [
                            'page-properties-column' => [
                                'class' => Column::class,
                                'settings' => [
                                    'm' => 5,
                                ],
                                'contents' => [
                                    'page-properties-card' => [
                                        'class' => Card::class,
                                        'settings' => [
                                            'title' => Yii::t('fafcms-sitemanager', 'Page properties'),
                                            'icon' => 'card-bulleted-settings-outline',
                                        ],
                                        'contents' => [
                                            'field-type' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.type',
                                                ],
                                            ],
                                            'field-layout_id' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.layout_id',
                                                ],
                                            ],
                                            'field-parent_contentmeta_id' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.parent_contentmeta_id',
                                                ],
                                            ],
                                            'field-title' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.title',
                                                ],
                                            ],
                                            'field-slug' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.slug',
                                                ],
                                            ],
                                            'field-description' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.description',
                                                ],
                                            ],
                                        ]
                                    ],
                                ]
                            ],
                        ],
                    ],
                ]
            ],
            'seo-tab' => [
                'class' => Tab::class,
                'settings' => [
                    'label' => ['fafcms-sitemanager', 'SEO & SMO'],
                ],
                'contents' => [
                    'row-1' => [
                        'class' => Row::class,
                        'contents' => [
                            'column-1' => [
                                'class' => Column::class,
                                'settings' => [
                                    'm' => 8,
                                ],
                                'contents' => [
                                    'card-1' => [
                                        'class' => Card::class,
                                        'settings' => [
                                            'title' => ['fafcms-sitemanager', 'SEO'],
                                            'icon' => 'cloud-search-outline',
                                        ],
                                        'contents' => [
                                            'field-sitemap_list' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.sitemap_list',
                                                ],
                                            ],
                                            'field-sitemap_priority' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.sitemap_priority',
                                                ],
                                            ],
                                            'field-sitemap_changefreq' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.sitemap_changefreq',
                                                ],
                                            ],
                                            'field-robots_disallow' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.robots_disallow',
                                                ],
                                            ],
                                            'field-robots_disallow_names' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.robots_disallow_names',
                                                ],
                                            ],
                                            'field-keywords' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.keywords',
                                                ],
                                            ],
                                        ]
                                    ],
                                ]
                            ],
                            'column-2' => [
                                'class' => Column::class,
                                'settings' => [
                                    'm' => 8,
                                ],
                                'contents' => [
                                    'card-1' => [
                                        'class' => Card::class,
                                        'settings' => [
                                            'title' => ['fafcms-sitemanager', 'SMO'],
                                            'icon' => 'share-variant-outline',
                                        ],
                                        'contents' => [
                                            'field-og_title' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_title',
                                                ],
                                            ],
                                            'field-og_type' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_type',
                                                ],
                                            ],
                                            'field-og_description' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_description',
                                                ],
                                            ],
                                            'field-og_determiner' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_determiner',
                                                ],
                                            ],
                                            'field-og_article_published_time' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_article_published_time',
                                                ],
                                            ],
                                            'field-og_article_modified_time' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_article_modified_time',
                                                ],
                                            ],
                                            'field-og_article_expiration_time' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_article_expiration_time',
                                                ],
                                            ],
                                            'field-og_article_section' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_article_section',
                                                ],
                                            ],
                                            'field-og_book_isbn' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_book_isbn',
                                                ],
                                            ],
                                            'field-og_book_release_date' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_book_release_date',
                                                ],
                                            ],
                                            'field-og_image' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.og_image',
                                                ],
                                            ],
                                        ]
                                    ]
                                ]
                            ],
                        ],
                    ],
                ]
            ],
            'categorization-tab' => [
                'class' => Tab::class,
                'settings' => [
                    'label' => ['category' => 'fafcms-core', 'message' => 'Categorization'],
                ],
                'contents' => [
                    'row-topics' => [
                        'class' => Row::class,
                        'contents' => [
                            'column-1' => [
                                'class' => Column::class,
                                'contents' => [
                                    'card-1' => [
                                        'class' => Card::class,
                                        'settings' => [
                                            'title' => Topic::instance()->getEditDataPlural(),
                                            'icon' => Topic::instance()->getEditDataIcon(),
                                        ],
                                        'contents' => [
                                            [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.topicids',
                                                    'hideLabel' => true,
                                                ],
                                            ],
                                        ]
                                    ],
                                ]
                            ],
                        ],
                    ],
                ]
            ]
        ];
    }

    /**
     * @return Closure
     */
    public function getEditViewButtonsContentmetaTrait(): Closure
    {
        return static function($buttons, $model, $form, $editView) {
            if (!$model->isNewRecord) {
                $buttons['open'] = [
                    'after' => 'save',
                    'icon' => 'card-search-outline',
                    'label' => Yii::t('fafcms-core', 'Open'),
                    'url' => $model->getAbsoluteUrl(),
                    'options' => [
                        'target' => '_blank',
                    ],
                ];
            }

            return $buttons;
        };
    }

    /**
     * Gets query for [[Contentmeta]].
     *
     * @return ActiveQuery
     */
    public function getContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, ['model_id' => 'id'])->andWhere([Contentmeta::tableName() . '.model_class' => self::class]);
    }

    /**
     * @param Contentmeta $relationModel
     * @param array       $relationRecordData
     */
    public function beforeSetContentmetaAttributesContentmetaTrait(Contentmeta $relationModel, array &$relationRecordData): void
    {
        $relationRecordData['model_class'] = self::class;

        if (method_exists($this, 'getProject_id')) {
            $relationRecordData['project_id'] = $this->getProject_id();
        } else {
            $relationRecordData['project_id'] = Yii::$app->fafcms->getCurrentProjectId();

        }

        if (method_exists($this, 'getProjectlanguage_id')) {
            $relationRecordData['projectlanguage_id'] = $this->getProjectlanguage_id();
        } else {
            $relationRecordData['projectlanguage_id'] = Yii::$app->fafcms->getCurrentProjectLanguageId();
        }

        $relationRecordData['translation_base_id'] = null;

        if (method_exists($this, 'getTranslation_base_id')) {
            $translationBaseId = $this->getTranslation_base_id();

            if ($translationBaseId !== null) {
                $translationBase = Contentmeta::find()
                    ->select('id')
                    ->where(['model_class' => self::class, 'model_id' => $translationBaseId])
                    ->byProjectLanguage('all')
                    ->scalar();

                if ($translationBase !== false) {
                    $relationRecordData['translation_base_id'] = $translationBase;
                }
            }
        }
    }

    /**
     * @param array|ActiveRecord $model
     * @param bool               $scheme
     *
     * @return string
     */
    public static function getUrl($model, bool $scheme = false): string
    {
        return Contentmeta::getUrl($model['contentmeta'], $scheme);
    }

    /**
     * @return string
     */
    public function getRelativeUrl(): string
    {
        return self::getUrl($this);
    }

    /**
     * @return string
     */
    public function getAbsoluteUrl(): string
    {
        return self::getUrl($this, true);
    }
}
