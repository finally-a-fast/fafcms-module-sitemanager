<?php

namespace fafcms\sitemanager;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\Layout;
use fafcms\sitemanager\models\Menu;
use fafcms\sitemanager\models\Menuitem;
use fafcms\sitemanager\models\Site;
use fafcms\sitemanager\models\Snippet;
use fafcms\sitemanager\models\Topic;
use yii\base\Application;
use Yii;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;
use yii\helpers\ArrayHelper;

class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-sitemanager';
    public static $tablePrefix = 'fafcms-site_';

    public static $backendUrlRules = [];

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-sitemanager'])) {
            $app->i18n->translations['fafcms-sitemanager'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        $module->accessRules = array_merge([
            'contentmeta' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
            'layout' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
            'site' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
            'snippet' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
            'topic' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
        ], $module->accessRules);

        if (isset($app->fafcmsParser)) {
            $app->fafcmsParser->data['allTopics'] = static function () {
                return ArrayHelper::index(Topic::find()->all(), 'id');
            };

            $app->fafcmsParser->data['allSites'] = static function () {
                return ArrayHelper::index(Site::find()->all(), 'id');
            };

            $app->fafcmsParser->data['allContentmetas'] = static function () {
                $contentmetaIds = Contentmeta::find()->select('id')->column();
                $contentmetaDataprovider = [];

                foreach ($contentmetaIds as $contentmetaId) {
                    $contentmetaDataprovider[$contentmetaId] = static function() use ($contentmetaId) {
                        return Contentmeta::find()->where(['id' => $contentmetaId])->one();
                    };
                }

                return $contentmetaDataprovider;
            };

            $menus = null;

            $app->fafcmsParser->data['menus'] = static function () use (&$menus, $module) {
                if ($menus === null) {
                    $rawMenus = Menu::find()
                        ->select('id, translation_base_id, name')
                        ->with(['menuitems'])
                        ->asArray()
                        ->all();

                    $menus = [];

                    foreach ($rawMenus as $rawMenu) {
                        $hasActiveChild = false;
                        $menus[$rawMenu['id']] = $rawMenu;
                        $menus[$rawMenu['id']]['menuitems'] = ArrayHelper::index($menus[$rawMenu['id']]['menuitems'], null, 'parent_menuitem_id');
                        $menus[$rawMenu['id']]['menuitems'] = $module->normalizeItems($menus[$rawMenu['id']]['menuitems'], null, $hasActiveChild);
                        $menus[$rawMenu['translation_base_id']] = $menus[$rawMenu['id']];
                    }
                }

                return $menus;
            };
        }

        Yii::$app->view->addNavigationItems(
            FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
            [
                'project' => [
                    'items' => [
                        'website' => [
                            'after' => 'project-dashboard',
                            'icon' => 'web',
                            'label' => ['fafcms-sitemanager', 'Website'],
                            'items' => [
                                'sites'    => [
                                    'icon'  => Site::instance()->getEditData()['icon'],
                                    'label' => Site::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Site::instance()->getEditData()['url'] . '/index'],
                                ],
                                'topic'    => [
                                    'icon'  => Topic::instance()->getEditData()['icon'],
                                    'label' => Topic::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Topic::instance()->getEditData()['url'] . '/index'],
                                ],
                                'layouts'  => [
                                    'icon'  => Layout::instance()->getEditData()['icon'],
                                    'label' => Layout::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Layout::instance()->getEditData()['url'] . '/index'],
                                ],
                                'snippets' => [
                                    'icon'  => Snippet::instance()->getEditData()['icon'],
                                    'label' => Snippet::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Snippet::instance()->getEditData()['url'] . '/index'],
                                ],
                                'menu'     => [
                                    'icon'  => Menu::instance()->getEditData()['icon'],
                                    'label' => Menu::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Menu::instance()->getEditData()['url'] . '/index'],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
        ]);

        return true;
    }
}
