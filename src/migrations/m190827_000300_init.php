<?php

namespace fafcms\sitemanager\migrations;

use yii\db\Migration;

/**
 * Class m190827_000300_init
 * @package fafcms\sitemanager\migrations
 */
class m190827_000300_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contentmeta}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'parent_contentmeta_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'title' => $this->string(255)->null()->defaultValue(null),
            'description' => $this->string(255)->null()->defaultValue(null),
            'slug' => $this->string(255)->notNull(),
            'model_class' => $this->string(255)->null()->defaultValue(null),
            'model_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'layout_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-contentmeta-parent_contentmeta_id', '{{%contentmeta}}', ['parent_contentmeta_id'], false);
        $this->createIndex('idx-contentmeta-model_id', '{{%contentmeta}}', ['model_id'], false);
        $this->createIndex('idx-contentmeta-layout_id', '{{%contentmeta}}', ['layout_id'], false);

        $this->createTable('{{%contentmeta_topic}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'contentmeta_id' => $this->integer(10)->unsigned()->notNull(),
            'topic_id' => $this->integer(10)->unsigned()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-contentmeta_topic-contentmeta_id', '{{%contentmeta_topic}}', ['contentmeta_id'], false);
        $this->createIndex('idx-contentmeta_topic-topic_id', '{{%contentmeta_topic}}', ['topic_id'], false);

        $this->createTable('{{%layout}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'content' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-layout-created_by', '{{%layout}}', ['created_by'], false);
        $this->createIndex('idx-layout-updated_by', '{{%layout}}', ['updated_by'], false);
        $this->createIndex('idx-layout-activated_by', '{{%layout}}', ['activated_by'], false);
        $this->createIndex('idx-layout-deactivated_by', '{{%layout}}', ['deactivated_by'], false);
        $this->createIndex('idx-layout-deleted_by', '{{%layout}}', ['deleted_by'], false);

        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-menu-created_by', '{{%menu}}', ['created_by'], false);
        $this->createIndex('idx-menu-updated_by', '{{%menu}}', ['updated_by'], false);
        $this->createIndex('idx-menu-activated_by', '{{%menu}}', ['activated_by'], false);
        $this->createIndex('idx-menu-deactivated_by', '{{%menu}}', ['deactivated_by'], false);
        $this->createIndex('idx-menu-deleted_by', '{{%menu}}', ['deleted_by'], false);

        $this->createTable('{{%menuitem}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'menu_id' => $this->integer(10)->unsigned()->notNull(),
            'parent_menuitem_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'position' => $this->integer(11)->null()->defaultValue(null),
            'contentmeta_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'type' => $this->string(255)->notNull()->defaultValue('content'),
            'attribute_download' => $this->string(255)->null()->defaultValue(null),
            'attribute_href' => $this->string(255)->null()->defaultValue(null),
            'attribute_hreflang' => $this->string(255)->null()->defaultValue(null),
            'attribute_media' => $this->string(255)->null()->defaultValue(null),
            'attribute_ping' => $this->string(255)->null()->defaultValue(null),
            'attribute_rel' => $this->string(255)->null()->defaultValue(null),
            'attribute_target' => $this->string(255)->null()->defaultValue(null),
            'attribute_type' => $this->string(255)->null()->defaultValue(null),
            'attribute_class' => $this->string(255)->null()->defaultValue(null),
            'attribute_id' => $this->string(255)->null()->defaultValue(null),
            'attribute_style' => $this->string(255)->null()->defaultValue(null),
            'attribute_tabindex' => $this->integer(11)->null()->defaultValue(null),
            'attribute_title' => $this->string(255)->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-menuitem-menu_id', '{{%menuitem}}', ['menu_id'], false);
        $this->createIndex('idx-menuitem-parent_menuitem_id', '{{%menuitem}}', ['parent_menuitem_id'], false);
        $this->createIndex('idx-menuitem-contentmeta_id', '{{%menuitem}}', ['contentmeta_id'], false);
        $this->createIndex('idx-menuitem-created_by', '{{%menuitem}}', ['created_by'], false);
        $this->createIndex('idx-menuitem-updated_by', '{{%menuitem}}', ['updated_by'], false);
        $this->createIndex('idx-menuitem-activated_by', '{{%menuitem}}', ['activated_by'], false);
        $this->createIndex('idx-menuitem-deactivated_by', '{{%menuitem}}', ['deactivated_by'], false);
        $this->createIndex('idx-menuitem-deleted_by', '{{%menuitem}}', ['deleted_by'], false);

        $this->createTable('{{%site}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'parent_site_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'content' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-site-parent_site_id', '{{%site}}', ['parent_site_id'], false);
        $this->createIndex('idx-site-created_by', '{{%site}}', ['created_by'], false);
        $this->createIndex('idx-site-updated_by', '{{%site}}', ['updated_by'], false);
        $this->createIndex('idx-site-activated_by', '{{%site}}', ['activated_by'], false);
        $this->createIndex('idx-site-deactivated_by', '{{%site}}', ['deactivated_by'], false);
        $this->createIndex('idx-site-deleted_by', '{{%site}}', ['deleted_by'], false);

        $this->createTable('{{%topic}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'parent_topic_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'site_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'content' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-topic-parent_topic_id', '{{%topic}}', ['parent_topic_id'], false);
        $this->createIndex('idx-topic-site_id', '{{%topic}}', ['site_id'], false);
        $this->createIndex('idx-topic-created_by', '{{%topic}}', ['created_by'], false);
        $this->createIndex('idx-topic-updated_by', '{{%topic}}', ['updated_by'], false);
        $this->createIndex('idx-topic-activated_by', '{{%topic}}', ['activated_by'], false);
        $this->createIndex('idx-topic-deactivated_by', '{{%topic}}', ['deactivated_by'], false);
        $this->createIndex('idx-topic-deleted_by', '{{%topic}}', ['deleted_by'], false);

        $this->addForeignKey('fk-contentmeta-parent_contentmeta_id', '{{%contentmeta}}', 'parent_contentmeta_id', '{{%contentmeta}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-contentmeta-layout_id', '{{%contentmeta}}', 'layout_id', '{{%layout}}', 'id', 'RESTRICT', 'CASCADE');

        $this->addForeignKey('fk-contentmeta_topic-contentmeta_id', '{{%contentmeta_topic}}', 'contentmeta_id', '{{%contentmeta}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-contentmeta_topic-topic_id', '{{%contentmeta_topic}}', 'topic_id', '{{%topic}}', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk-layout-created_by', '{{%layout}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-layout-updated_by', '{{%layout}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-layout-activated_by', '{{%layout}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-layout-deactivated_by', '{{%layout}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-layout-deleted_by', '{{%layout}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-menu-created_by', '{{%menu}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-menu-updated_by', '{{%menu}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-menu-activated_by', '{{%menu}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-menu-deactivated_by', '{{%menu}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-menu-deleted_by', '{{%menu}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-menuitem-menu_id', '{{%menuitem}}', 'menu_id', '{{%menu}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-menuitem-parent_menuitem_id', '{{%menuitem}}', 'parent_menuitem_id', '{{%menuitem}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-menuitem-contentmeta_id', '{{%menuitem}}', 'contentmeta_id', '{{%contentmeta}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-menuitem-created_by', '{{%menuitem}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-menuitem-updated_by', '{{%menuitem}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-menuitem-activated_by', '{{%menuitem}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-menuitem-deactivated_by', '{{%menuitem}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-menuitem-deleted_by', '{{%menuitem}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-site-parent_site_id', '{{%site}}', 'parent_site_id', '{{%site}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-site-created_by', '{{%site}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-site-updated_by', '{{%site}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-site-activated_by', '{{%site}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-site-deactivated_by', '{{%site}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-site-deleted_by', '{{%site}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-topic-parent_topic_id', '{{%topic}}', 'parent_topic_id', '{{%topic}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-topic-site_id', '{{%topic}}', 'site_id', '{{%site}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-topic-created_by', '{{%topic}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-topic-updated_by', '{{%topic}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-topic-activated_by', '{{%topic}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-topic-deactivated_by', '{{%topic}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-topic-deleted_by', '{{%topic}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-contentmeta-parent_contentmeta_id', '{{%contentmeta}}');
        $this->dropForeignKey('fk-contentmeta-layout_id', '{{%contentmeta}}');

        $this->dropForeignKey('fk-contentmeta_topic-contentmeta_id', '{{%contentmeta_topic}}');
        $this->dropForeignKey('fk-contentmeta_topic-topic_id', '{{%contentmeta_topic}}');

        $this->dropForeignKey('fk-layout-created_by', '{{%layout}}');
        $this->dropForeignKey('fk-layout-updated_by', '{{%layout}}');
        $this->dropForeignKey('fk-layout-activated_by', '{{%layout}}');
        $this->dropForeignKey('fk-layout-deactivated_by', '{{%layout}}');
        $this->dropForeignKey('fk-layout-deleted_by', '{{%layout}}');

        $this->dropForeignKey('fk-menu-created_by', '{{%menu}}');
        $this->dropForeignKey('fk-menu-updated_by', '{{%menu}}');
        $this->dropForeignKey('fk-menu-activated_by', '{{%menu}}');
        $this->dropForeignKey('fk-menu-deactivated_by', '{{%menu}}');
        $this->dropForeignKey('fk-menu-deleted_by', '{{%menu}}');

        $this->dropForeignKey('fk-menuitem-menu_id', '{{%menuitem}}');
        $this->dropForeignKey('fk-menuitem-parent_menuitem_id', '{{%menuitem}}');
        $this->dropForeignKey('fk-menuitem-contentmeta_id', '{{%menuitem}}');
        $this->dropForeignKey('fk-menuitem-created_by', '{{%menuitem}}');
        $this->dropForeignKey('fk-menuitem-updated_by', '{{%menuitem}}');
        $this->dropForeignKey('fk-menuitem-activated_by', '{{%menuitem}}');
        $this->dropForeignKey('fk-menuitem-deactivated_by', '{{%menuitem}}');
        $this->dropForeignKey('fk-menuitem-deleted_by', '{{%menuitem}}');

        $this->dropForeignKey('fk-site-parent_site_id', '{{%site}}');
        $this->dropForeignKey('fk-site-created_by', '{{%site}}');
        $this->dropForeignKey('fk-site-updated_by', '{{%site}}');
        $this->dropForeignKey('fk-site-activated_by', '{{%site}}');
        $this->dropForeignKey('fk-site-deactivated_by', '{{%site}}');
        $this->dropForeignKey('fk-site-deleted_by', '{{%site}}');

        $this->dropForeignKey('fk-topic-parent_topic_id', '{{%topic}}');
        $this->dropForeignKey('fk-topic-site_id', '{{%topic}}');
        $this->dropForeignKey('fk-topic-created_by', '{{%topic}}');
        $this->dropForeignKey('fk-topic-updated_by', '{{%topic}}');
        $this->dropForeignKey('fk-topic-activated_by', '{{%topic}}');
        $this->dropForeignKey('fk-topic-deactivated_by', '{{%topic}}');
        $this->dropForeignKey('fk-topic-deleted_by', '{{%topic}}');

        $this->dropTable('{{%contentmeta}}');
        $this->dropTable('{{%contentmeta_topic}}');
        $this->dropTable('{{%layout}}');
        $this->dropTable('{{%menu}}');
        $this->dropTable('{{%menuitem}}');
        $this->dropTable('{{%site}}');
        $this->dropTable('{{%topic}}');
    }
}
