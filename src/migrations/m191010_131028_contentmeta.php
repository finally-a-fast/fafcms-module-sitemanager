<?php

namespace fafcms\sitemanager\migrations;

use yii\db\Migration;

/**
 * Class m191010_131028_contentmeta
 * @package fafcms\sitemanager\migrations
 */
class m191010_131028_contentmeta extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contentmeta}}', 'type', $this->string(255)->notNull()->defaultValue('content')->after('parent_contentmeta_id'));
        $this->addColumn('{{%contentmeta}}', 'keywords', $this->text()->null()->defaultValue(null)->after('description'));
        $this->addColumn('{{%contentmeta}}', 'sitemap_list', $this->tinyInteger(1)->notNull()->defaultValue(1)->after('keywords'));
        $this->addColumn('{{%contentmeta}}', 'sitemap_changefreq', $this->tinyInteger(1)->notNull()->defaultValue(5)->after('sitemap_list'));
        $this->addColumn('{{%contentmeta}}', 'sitemap_priority', $this->string(255)->notNull()->defaultValue('weekly')->after('sitemap_changefreq'));
        $this->addColumn('{{%contentmeta}}', 'robots_disallow', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('sitemap_priority'));
        $this->addColumn('{{%contentmeta}}', 'robots_disallow_names', $this->string(255)->null()->defaultValue(null)->after('robots_disallow'));

        $this->alterColumn('{{%contentmeta}}', 'slug', $this->string(255)->null()->defaultValue(null));
        $this->alterColumn('{{%contentmeta}}', 'description', $this->text()->null()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contentmeta}}', 'type');
        $this->dropColumn('{{%contentmeta}}', 'keywords');
        $this->dropColumn('{{%contentmeta}}', 'sitemap_list');
        $this->dropColumn('{{%contentmeta}}', 'sitemap_changefreq');
        $this->dropColumn('{{%contentmeta}}', 'sitemap_priority');
        $this->dropColumn('{{%contentmeta}}', 'robots_disallow');
        $this->dropColumn('{{%contentmeta}}', 'robots_disallow_names');

        $this->alterColumn('{{%contentmeta}}', 'slug', $this->string(255)->notNull());
        $this->alterColumn('{{%contentmeta}}', 'description', $this->string(255)->null()->defaultValue(null));
    }
}
