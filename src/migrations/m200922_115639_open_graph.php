<?php

namespace fafcms\sitemanager\migrations;

use fafcms\sitemanager\models\Contentmeta;
use yii\db\Migration;

/**
 * Class m200922_115639_open_graph
 *
 * @package fafcms\sitemanager\migrations
 */
class m200922_115639_open_graph extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Contentmeta::tableName(), 'og_title', $this->string(255)->null()->defaultValue(null)->after('keywords'));
        $this->addColumn(Contentmeta::tableName(), 'og_type', $this->string(255)->notNull()->defaultValue('website')->after('og_title'));
        $this->addColumn(Contentmeta::tableName(), 'og_image', $this->string(255)->null()->defaultValue(null)->after('og_type'));
        $this->addColumn(Contentmeta::tableName(), 'og_description', $this->string(255)->null()->defaultValue(null)->after('og_image'));
        $this->addColumn(Contentmeta::tableName(), 'og_determiner', $this->string(255)->null()->defaultValue(null)->after('og_description'));
        $this->addColumn(Contentmeta::tableName(), 'og_article_published_time', $this->datetime()->null()->defaultValue(null)->after('og_determiner'));
        $this->addColumn(Contentmeta::tableName(), 'og_article_modified_time', $this->datetime()->null()->defaultValue(null)->after('og_article_published_time'));
        $this->addColumn(Contentmeta::tableName(), 'og_article_expiration_time', $this->datetime()->null()->defaultValue(null)->after('og_article_modified_time'));
        $this->addColumn(Contentmeta::tableName(), 'og_article_section', $this->string(255)->null()->defaultValue(null)->after('og_article_expiration_time'));
        $this->addColumn(Contentmeta::tableName(), 'og_book_isbn', $this->string(255)->null()->defaultValue(null)->after('og_article_section'));
        $this->addColumn(Contentmeta::tableName(), 'og_book_release_date', $this->datetime()->null()->defaultValue(null)->after('og_book_isbn'));
    }

    public function safeDown()
    {
        $this->dropColumn(Contentmeta::tableName(), 'og_title');
        $this->dropColumn(Contentmeta::tableName(), 'og_type');
        $this->dropColumn(Contentmeta::tableName(), 'og_image');
        $this->dropColumn(Contentmeta::tableName(), 'og_description');
        $this->dropColumn(Contentmeta::tableName(), 'og_determiner');
        $this->dropColumn(Contentmeta::tableName(), 'og_article_published_time');
        $this->dropColumn(Contentmeta::tableName(), 'og_article_modified_time');
        $this->dropColumn(Contentmeta::tableName(), 'og_article_expiration_time');
        $this->dropColumn(Contentmeta::tableName(), 'og_article_section');
        $this->dropColumn(Contentmeta::tableName(), 'og_book_isbn');
        $this->dropColumn(Contentmeta::tableName(), 'og_book_release_date');
    }
}
