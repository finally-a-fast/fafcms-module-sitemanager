<?php

namespace fafcms\sitemanager\migrations;

use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\ContentmetaTopic;
use fafcms\sitemanager\models\Layout;
use fafcms\sitemanager\models\Menu;
use fafcms\sitemanager\models\Menuitem;
use fafcms\sitemanager\models\Site;
use fafcms\sitemanager\models\Topic;
use yii\db\Migration;

/**
 * Class m200120_210709_prefix
 * @package fafcms\sitemanager\migrations
 */
class m200120_210709_prefix extends Migration
{
    public function safeUp()
    {
        $this->renameTable('{{%contentmeta}}', Contentmeta::tableName());
        $this->renameTable('{{%contentmeta_topic}}', ContentmetaTopic::tableName());
        $this->renameTable('{{%layout}}', Layout::tableName());
        $this->renameTable('{{%menu}}', Menu::tableName());
        $this->renameTable('{{%menuitem}}', Menuitem::tableName());
        $this->renameTable('{{%site}}', Site::tableName());
        $this->renameTable('{{%topic}}', Topic::tableName());
    }

    public function safeDown()
    {
        $this->renameTable(Contentmeta::tableName(), '{{%contentmeta}}');
        $this->renameTable(ContentmetaTopic::tableName(), '{{%contentmeta_topic}}');
        $this->renameTable(Layout::tableName(), '{{%layout}}');
        $this->renameTable(Menu::tableName(), '{{%menu}}');
        $this->renameTable(Menuitem::tableName(), '{{%menuitem}}');
        $this->renameTable(Site::tableName(), '{{%site}}');
        $this->renameTable(Topic::tableName(), '{{%topic}}');
    }
}
