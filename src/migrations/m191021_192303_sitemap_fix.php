<?php

namespace fafcms\sitemanager\migrations;

use yii\db\Migration;

/**
 * Class m191021_192303_sitemap_fix
 * @package fafcms\sitemanager\migrations
 */
class m191021_192303_sitemap_fix extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%contentmeta}}', 'sitemap_changefreq', 'sitemap_priority_temp');
        $this->renameColumn('{{%contentmeta}}', 'sitemap_priority', 'sitemap_changefreq');
        $this->renameColumn('{{%contentmeta}}', 'sitemap_priority_temp', 'sitemap_priority');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%contentmeta}}', 'sitemap_changefreq', 'sitemap_priority_temp');
        $this->renameColumn('{{%contentmeta}}', 'sitemap_priority', 'sitemap_changefreq');
        $this->renameColumn('{{%contentmeta}}', 'sitemap_priority_temp', 'sitemap_priority');
    }
}
