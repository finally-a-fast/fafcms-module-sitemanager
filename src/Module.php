<?php

namespace fafcms\sitemanager;

use fafcms\fafcms\controllers\FrontendController;
use Yii;
use yii\helpers\FormatConverter;
use yii\helpers\Url;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\Checkbox;
use DateTimeZone;
use DateTime;

class Module extends PluginModule
{
    public function getPluginSettingRules(): array
    {
        return [
            [['enable_snippet_cache'], 'boolean'],
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'enable_snippet_cache',
                'label' => Yii::t('fafcms-sitemanager', 'Enable snippet cache'),
                'defaultValue' => true,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
        ];
    }

    /**
     * @param array $allItems
     * @param int|null $activeId
     * @param bool $active
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function normalizeItems(array $allItems, ?int $activeId, bool &$active):array
    {
        $items = $allItems[$activeId] ?? [];

        for ($i = 0, $itemCount = count($items); $i < $itemCount; $i++) {
            $hasActiveChild = false;
            $itemActive = false;

            $items[$i]['menuitems'] = $this->normalizeItems($allItems, $items[$i]['id'], $hasActiveChild);

            $url = $items[$i]['attribute_href'];
            $contentmetaId = null;
            $contentModel = null;

            if ($items[$i]['type'] === 'content') {
                $contentmetaId = $items[$i]['contentmeta_id'];
                $url = Url::to('/' . Contentmeta::getUrlById($items[$i]['contentmeta_id']));
            } elseif (mb_strpos($url, '://') === false) {
                $contentmetaId = Contentmeta::getIdByUrl(ltrim(mb_substr($url, 0, mb_strpos($url, '#')), '/'), false, false,true);
            }

            if ($contentmetaId !== null) {
                $contentModel = Contentmeta::getContentModelById($contentmetaId, true, 'no-status-filter', function($query) {
                    return $query->byStatus('all', false, false)->one();
                });
            }

            if ($contentModel !== null && $items[$i]['name'] === 'Unternehmensporträt') {
                $remove = false;

                if (($contentModel['status'] ?? 'active') === 'inactive') {
                    $remove = true;
                } else {
                    $timeZone = 'UTC';
                    $datetimeFormat = 'medium';

                    if ((Yii::$app->formatter ?? null) !== null) {
                        $timeZone = Yii::$app->formatter->defaultTimeZone;
                        $datetimeFormat = Yii::$app->formatter->datetimeFormat;
                    }

                    $timeZone = new DateTimeZone($timeZone);
                    $previewTime = new DateTime('now', $timeZone);

                    if (!YII_CONSOLE && !Yii::$app->user->getIsGuest() && !empty(Yii::$app->session->get('preview_show_date'))) {
                        $previewTime = DateTime::createFromFormat(FormatConverter::convertDateIcuToPhp($datetimeFormat, 'datetime'), Yii::$app->session->get('preview_show_date'), $timeZone);
                    }

                    if (($contentModel['display_start'] ?? null) !== null) {
                        $startTime = DateTime::createFromFormat('Y-m-d H:i:s', $contentModel['display_start'], $timeZone);

                        if ($previewTime < $startTime) {
                            $remove = true;
                        }
                    }

                    if ($remove === false && ($contentModel['display_end'] ?? null) !== null) {
                        $endTime = DateTime::createFromFormat('Y-m-d H:i:s', $contentModel['display_end'], $timeZone);

                        if ($previewTime > $endTime) {
                            $remove = true;
                        }
                    }
                }

                if ($remove) {
                    unset($items[$i]);
                    continue;
                }
            }

            if ($hasActiveChild || $this->isItemActive($url)) {
                $active = true;
                $itemActive = true;
            }

            $items[$i]['url'] = $url;
            $items[$i]['active'] = $itemActive;
        }

        usort($items, function($a, $b) {
            return $a['position'] <=> $b['position'];
        });

        return $items;
    }

    /**
     * @param string $url
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    private function isItemActive(string $url):bool
    {
        $requestRoute = ltrim(Yii::$app->request->getUrl(), '/');
        $serverName = Yii::$app->request->getServerName();

        $url = ltrim(preg_replace('/^(?:https?:\/\/)?'.$serverName.'/', '', $url), '/');

        $url = explode('?', $url, 2);
        $requestRoute = explode('?', $requestRoute, 2);

        if ($url[0] !== $requestRoute[0]) {
            return false;
        }

        if (isset($url[1])) {
            if (!isset($requestRoute[1])) {
                return false;
            }

            $url = explode('#', $url[1], 2)[0];
            $requestRoute = explode('#', $requestRoute[1], 2)[0];

            $urlParams = [];
            $requestParams = [];

            foreach (explode('&', $url) as $param) {
                $param = explode('=', $param);
                $urlParams[$param[0]] = $param[1]??'';
            }

            foreach (explode('&', $requestRoute) as $param) {
                $param = explode('=', $param);
                $requestParams[$param[0]] = $param[1]??'';
            }

            foreach ($urlParams as $name => $value) {
                if ($value !== '' && (!isset($requestParams[$name]) || $requestParams[$name] != $value)) {
                    return false;
                }
            }
        }

        return true;
    }
}
