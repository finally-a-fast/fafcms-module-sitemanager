<?php

namespace fafcms\sitemanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\Project,
    models\Projectlanguage,
};
use fafcms\filemanager\inputs\FileSelect;
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\shariff\models\Shariffcontentmeta;
use fafcms\sitemanager\{
    models\ContentmetaTopic,
    models\Layout,
    models\Menuitem,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%contentmeta}}".
 *
 * @package fafcms\sitemanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int|null $parent_contentmeta_id
 * @property string $type
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property string|null $og_title
 * @property string $og_type
 * @property string|null $og_image
 * @property string|null $og_description
 * @property string|null $og_determiner
 * @property string|null $og_article_published_time
 * @property string|null $og_article_modified_time
 * @property string|null $og_article_expiration_time
 * @property string|null $og_article_section
 * @property string|null $og_book_isbn
 * @property string|null $og_book_release_date
 * @property int $sitemap_list
 * @property int $sitemap_priority
 * @property string $sitemap_changefreq
 * @property int $robots_disallow
 * @property string|null $robots_disallow_names
 * @property string|null $slug
 * @property string|null $model_class
 * @property int|null $model_id
 * @property int|null $layout_id
 *
 * @property ContentmetaTopic[] $contentmetaContentmetaTopics
 * @property Menuitem[] $contentmetaMenuitems
 * @property Shariffcontentmeta[] $contentmetaShariffcontentmetas
 * @property Projectlanguage[] $errorPageContentmetaProjectlanguages
 * @property Project[] $errorPageContentmetaProjects
 * @property Layout $layout
 * @property Contentmeta $parentContentmeta
 * @property Contentmeta[] $parentContentmetaContentmetas
 * @property Projectlanguage[] $startPageContentmetaProjectlanguages
 * @property Project[] $startPageContentmetaProjects
 */
abstract class BaseContentmeta extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'contentmeta';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'contentmeta';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-sitemanager', 'Contentmetas');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-sitemanager', 'Contentmetum');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['title'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.title'
            ])
            ->setSort([static::tableName() . '.title' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'parent_contentmeta_id' => static function($properties = []) {
                return static::getOptionProvider($properties)->getOptions();
            },
            'layout_id' => static function($properties = []) {
                return Layout::getOptionProvider($properties)->getOptions();
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'parent_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('parent_contentmeta_id', false),
                'relationClassName' => static::class,
            ],
            'type' => [
                'type' => TextInput::class,
            ],
            'title' => [
                'type' => TextInput::class,
            ],
            'description' => [
                'type' => Textarea::class,
            ],
            'keywords' => [
                'type' => Textarea::class,
            ],
            'og_title' => [
                'type' => TextInput::class,
            ],
            'og_type' => [
                'type' => TextInput::class,
            ],
            'og_image' => [
                'type' => FileSelect::class,
                'mediatypes' => [
                    'image',
                ],
            ],
            'og_description' => [
                'type' => TextInput::class,
            ],
            'og_determiner' => [
                'type' => TextInput::class,
            ],
            'og_article_published_time' => [
                'type' => DateTimePicker::class,
            ],
            'og_article_modified_time' => [
                'type' => DateTimePicker::class,
            ],
            'og_article_expiration_time' => [
                'type' => DateTimePicker::class,
            ],
            'og_article_section' => [
                'type' => TextInput::class,
            ],
            'og_book_isbn' => [
                'type' => TextInput::class,
            ],
            'og_book_release_date' => [
                'type' => DateTimePicker::class,
            ],
            'sitemap_list' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'sitemap_priority' => [
                'type' => NumberInput::class,
            ],
            'sitemap_changefreq' => [
                'type' => TextInput::class,
            ],
            'robots_disallow' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'robots_disallow_names' => [
                'type' => TextInput::class,
            ],
            'slug' => [
                'type' => TextInput::class,
            ],
            'model_class' => [
                'type' => TextInput::class,
            ],
            'model_id' => [
                'type' => NumberInput::class,
            ],
            'layout_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('layout_id', false),
                'relationClassName' => Layout::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'parent_contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'parent_contentmeta_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'title',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'type' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'type',
                        'sort' => 4,
                    ],
                ],
                'description' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'description',
                        'sort' => 5,
                    ],
                ],
                'keywords' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'keywords',
                        'sort' => 6,
                    ],
                ],
                'og_title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_title',
                        'sort' => 7,
                    ],
                ],
                'og_type' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_type',
                        'sort' => 8,
                    ],
                ],
                'og_image' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_image',
                        'sort' => 9,
                    ],
                ],
                'og_description' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_description',
                        'sort' => 10,
                    ],
                ],
                'og_determiner' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_determiner',
                        'sort' => 11,
                    ],
                ],
                'og_article_published_time' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_article_published_time',
                        'sort' => 12,
                    ],
                ],
                'og_article_modified_time' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_article_modified_time',
                        'sort' => 13,
                    ],
                ],
                'og_article_expiration_time' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_article_expiration_time',
                        'sort' => 14,
                    ],
                ],
                'og_article_section' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_article_section',
                        'sort' => 15,
                    ],
                ],
                'og_book_isbn' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_book_isbn',
                        'sort' => 16,
                    ],
                ],
                'og_book_release_date' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_book_release_date',
                        'sort' => 17,
                    ],
                ],
                'sitemap_list' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sitemap_list',
                        'sort' => 18,
                    ],
                ],
                'sitemap_priority' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sitemap_priority',
                        'sort' => 19,
                    ],
                ],
                'sitemap_changefreq' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sitemap_changefreq',
                        'sort' => 20,
                    ],
                ],
                'robots_disallow' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'robots_disallow',
                        'sort' => 21,
                    ],
                ],
                'robots_disallow_names' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'robots_disallow_names',
                        'sort' => 22,
                    ],
                ],
                'slug' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'slug',
                        'sort' => 23,
                    ],
                ],
                'model_class' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'model_class',
                        'sort' => 24,
                    ],
                ],
                'model_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'model_id',
                        'sort' => 25,
                    ],
                ],
                'layout_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'layout_id',
                        'sort' => 26,
                        'link' => true,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-parent_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'parent_contentmeta_id',
                                                    ],
                                                ],
                                                'field-type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'type',
                                                    ],
                                                ],
                                                'field-title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                'field-description' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'description',
                                                    ],
                                                ],
                                                'field-keywords' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'keywords',
                                                    ],
                                                ],
                                                'field-og_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_title',
                                                    ],
                                                ],
                                                'field-og_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_type',
                                                    ],
                                                ],
                                                'field-og_image' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_image',
                                                    ],
                                                ],
                                                'field-og_description' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_description',
                                                    ],
                                                ],
                                                'field-og_determiner' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_determiner',
                                                    ],
                                                ],
                                                'field-og_article_published_time' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_article_published_time',
                                                    ],
                                                ],
                                                'field-og_article_modified_time' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_article_modified_time',
                                                    ],
                                                ],
                                                'field-og_article_expiration_time' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_article_expiration_time',
                                                    ],
                                                ],
                                                'field-og_article_section' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_article_section',
                                                    ],
                                                ],
                                                'field-og_book_isbn' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_book_isbn',
                                                    ],
                                                ],
                                                'field-og_book_release_date' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_book_release_date',
                                                    ],
                                                ],
                                                'field-sitemap_list' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sitemap_list',
                                                    ],
                                                ],
                                                'field-sitemap_priority' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sitemap_priority',
                                                    ],
                                                ],
                                                'field-sitemap_changefreq' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sitemap_changefreq',
                                                    ],
                                                ],
                                                'field-robots_disallow' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'robots_disallow',
                                                    ],
                                                ],
                                                'field-robots_disallow_names' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'robots_disallow_names',
                                                    ],
                                                ],
                                                'field-slug' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'slug',
                                                    ],
                                                ],
                                                'field-model_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'model_class',
                                                    ],
                                                ],
                                                'field-model_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'model_id',
                                                    ],
                                                ],
                                                'field-layout_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'layout_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%contentmeta}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-parent_contentmeta_id' => ['parent_contentmeta_id', 'integer'],
            'integer-sitemap_priority' => ['sitemap_priority', 'integer'],
            'integer-model_id' => ['model_id', 'integer'],
            'integer-layout_id' => ['layout_id', 'integer'],
            'string-description' => ['description', 'string'],
            'string-keywords' => ['keywords', 'string'],
            'date-og_article_published_time' => ['og_article_published_time', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'og_article_published_time', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-og_article_modified_time' => ['og_article_modified_time', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'og_article_modified_time', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-og_article_expiration_time' => ['og_article_expiration_time', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'og_article_expiration_time', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-og_book_release_date' => ['og_book_release_date', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'og_book_release_date', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'boolean-sitemap_list' => ['sitemap_list', 'boolean'],
            'boolean-robots_disallow' => ['robots_disallow', 'boolean'],
            'string-type' => ['type', 'string', 'max' => 255],
            'string-title' => ['title', 'string', 'max' => 255],
            'string-og_title' => ['og_title', 'string', 'max' => 255],
            'string-og_type' => ['og_type', 'string', 'max' => 255],
            'string-og_image' => ['og_image', 'string', 'max' => 255],
            'string-og_description' => ['og_description', 'string', 'max' => 255],
            'string-og_determiner' => ['og_determiner', 'string', 'max' => 255],
            'string-og_article_section' => ['og_article_section', 'string', 'max' => 255],
            'string-og_book_isbn' => ['og_book_isbn', 'string', 'max' => 255],
            'string-sitemap_changefreq' => ['sitemap_changefreq', 'string', 'max' => 255],
            'string-robots_disallow_names' => ['robots_disallow_names', 'string', 'max' => 255],
            'string-slug' => ['slug', 'string', 'max' => 255],
            'string-model_class' => ['model_class', 'string', 'max' => 255],
            'exist-layout_id' => [['layout_id'], 'exist', 'skipOnError' => true, 'targetClass' => Layout::class, 'targetAttribute' => ['layout_id' => 'id']],
            'exist-parent_contentmeta_id' => [['parent_contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_contentmeta_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-sitemanager', 'ID'),
            'parent_contentmeta_id' => Yii::t('fafcms-sitemanager', 'Parent Contentmeta ID'),
            'type' => Yii::t('fafcms-sitemanager', 'Type'),
            'title' => Yii::t('fafcms-sitemanager', 'Title'),
            'description' => Yii::t('fafcms-sitemanager', 'Description'),
            'keywords' => Yii::t('fafcms-sitemanager', 'Keywords'),
            'og_title' => Yii::t('fafcms-sitemanager', 'Og Title'),
            'og_type' => Yii::t('fafcms-sitemanager', 'Og Type'),
            'og_image' => Yii::t('fafcms-sitemanager', 'Og Image'),
            'og_description' => Yii::t('fafcms-sitemanager', 'Og Description'),
            'og_determiner' => Yii::t('fafcms-sitemanager', 'Og Determiner'),
            'og_article_published_time' => Yii::t('fafcms-sitemanager', 'Og Article Published Time'),
            'og_article_modified_time' => Yii::t('fafcms-sitemanager', 'Og Article Modified Time'),
            'og_article_expiration_time' => Yii::t('fafcms-sitemanager', 'Og Article Expiration Time'),
            'og_article_section' => Yii::t('fafcms-sitemanager', 'Og Article Section'),
            'og_book_isbn' => Yii::t('fafcms-sitemanager', 'Og Book Isbn'),
            'og_book_release_date' => Yii::t('fafcms-sitemanager', 'Og Book Release Date'),
            'sitemap_list' => Yii::t('fafcms-sitemanager', 'Sitemap List'),
            'sitemap_priority' => Yii::t('fafcms-sitemanager', 'Sitemap Priority'),
            'sitemap_changefreq' => Yii::t('fafcms-sitemanager', 'Sitemap Changefreq'),
            'robots_disallow' => Yii::t('fafcms-sitemanager', 'Robots Disallow'),
            'robots_disallow_names' => Yii::t('fafcms-sitemanager', 'Robots Disallow Names'),
            'slug' => Yii::t('fafcms-sitemanager', 'Slug'),
            'model_class' => Yii::t('fafcms-sitemanager', 'Model Class'),
            'model_id' => Yii::t('fafcms-sitemanager', 'Model ID'),
            'layout_id' => Yii::t('fafcms-sitemanager', 'Layout ID'),
        ]);
    }

    /**
     * Gets query for [[ContentmetaContentmetaTopics]].
     *
     * @return ActiveQuery
     */
    public function getContentmetaContentmetaTopics(): ActiveQuery
    {
        return $this->hasMany(ContentmetaTopic::class, [
            'contentmeta_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ContentmetaMenuitems]].
     *
     * @return ActiveQuery
     */
    public function getContentmetaMenuitems(): ActiveQuery
    {
        return $this->hasMany(Menuitem::class, [
            'contentmeta_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ContentmetaShariffcontentmetas]].
     *
     * @return ActiveQuery
     */
    public function getContentmetaShariffcontentmetas(): ActiveQuery
    {
        return $this->hasMany(Shariffcontentmeta::class, [
            'contentmeta_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ErrorPageContentmetaProjectlanguages]].
     *
     * @return ActiveQuery
     */
    public function getErrorPageContentmetaProjectlanguages(): ActiveQuery
    {
        return $this->hasMany(Projectlanguage::class, [
            'error_page_contentmeta_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ErrorPageContentmetaProjects]].
     *
     * @return ActiveQuery
     */
    public function getErrorPageContentmetaProjects(): ActiveQuery
    {
        return $this->hasMany(Project::class, [
            'error_page_contentmeta_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Layout]].
     *
     * @return ActiveQuery
     */
    public function getLayout(): ActiveQuery
    {
        return $this->hasOne(Layout::class, [
            'id' => 'layout_id',
        ]);
    }

    /**
     * Gets query for [[ParentContentmeta]].
     *
     * @return ActiveQuery
     */
    public function getParentContentmeta(): ActiveQuery
    {
        return $this->hasOne(static::class, [
            'id' => 'parent_contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[ParentContentmetaContentmetas]].
     *
     * @return ActiveQuery
     */
    public function getParentContentmetaContentmetas(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'parent_contentmeta_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[StartPageContentmetaProjectlanguages]].
     *
     * @return ActiveQuery
     */
    public function getStartPageContentmetaProjectlanguages(): ActiveQuery
    {
        return $this->hasMany(Projectlanguage::class, [
            'start_page_contentmeta_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[StartPageContentmetaProjects]].
     *
     * @return ActiveQuery
     */
    public function getStartPageContentmetaProjects(): ActiveQuery
    {
        return $this->hasMany(Project::class, [
            'start_page_contentmeta_id' => 'id',
        ]);
    }
}
