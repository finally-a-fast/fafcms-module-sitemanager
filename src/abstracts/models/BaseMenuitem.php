<?php

namespace fafcms\sitemanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\NumberInput,
    inputs\ExtendedDropDownList,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\filemanager\inputs\FileSelect;
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\sitemanager\{
    models\Contentmeta,
    models\Menu,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%menuitem}}".
 *
 * @package fafcms\sitemanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string|null $display_start
 * @property string|null $display_end
 * @property int $menu_id
 * @property int|null $parent_menuitem_id
 * @property string $name
 * @property int|null $position
 * @property int|null $contentmeta_id
 * @property string $type
 * @property string|null $attribute_download
 * @property string|null $attribute_href
 * @property string|null $attribute_hreflang
 * @property string|null $attribute_media
 * @property string|null $attribute_ping
 * @property string|null $attribute_rel
 * @property string|null $attribute_target
 * @property string|null $attribute_type
 * @property string|null $attribute_class
 * @property string|null $attribute_id
 * @property string|null $attribute_style
 * @property int|null $attribute_tabindex
 * @property string|null $attribute_title
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property Contentmeta $contentmeta
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property Menu $menu
 * @property Menuitem $parentMenuitem
 * @property Menuitem[] $parentMenuitemMenuitems
 * @property User $updatedBy
 */
abstract class BaseMenuitem extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'menuitem';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'menuitem';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-site', 'Menuitems');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-site', 'Menuitem');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'menu_id' => static function($properties = []) {
                return Menu::getOptionProvider($properties)->getOptions();
            },
            'parent_menuitem_id' => static function($properties = []) {
                return static::getOptionProvider($properties)->getOptions();
            },
            'contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'display_start' => [
                'type' => DateTimePicker::class,
            ],
            'display_end' => [
                'type' => DateTimePicker::class,
            ],
            'menu_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('menu_id', false),
                'relationClassName' => Menu::class,
            ],
            'parent_menuitem_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('parent_menuitem_id', false),
                'relationClassName' => static::class,
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'position' => [
                'type' => NumberInput::class,
            ],
            'contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'type' => [
                'type' => TextInput::class,
            ],
            'attribute_download' => [
                'type' => TextInput::class,
            ],
            'attribute_href' => [
                'type' => TextInput::class,
            ],
            'attribute_hreflang' => [
                'type' => TextInput::class,
            ],
            'attribute_media' => [
                'type' => FileSelect::class,
            ],
            'attribute_ping' => [
                'type' => TextInput::class,
            ],
            'attribute_rel' => [
                'type' => TextInput::class,
            ],
            'attribute_target' => [
                'type' => TextInput::class,
            ],
            'attribute_type' => [
                'type' => TextInput::class,
            ],
            'attribute_class' => [
                'type' => TextInput::class,
            ],
            'attribute_id' => [
                'type' => TextInput::class,
            ],
            'attribute_style' => [
                'type' => TextInput::class,
            ],
            'attribute_tabindex' => [
                'type' => NumberInput::class,
            ],
            'attribute_title' => [
                'type' => TextInput::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('activated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deactivated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deleted_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'display_start' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'display_start',
                        'sort' => 4,
                    ],
                ],
                'display_end' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'display_end',
                        'sort' => 5,
                    ],
                ],
                'menu_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'menu_id',
                        'sort' => 6,
                        'link' => true,
                    ],
                ],
                'parent_menuitem_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'parent_menuitem_id',
                        'sort' => 7,
                        'link' => true,
                    ],
                ],
                'position' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'position',
                        'sort' => 8,
                    ],
                ],
                'contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'contentmeta_id',
                        'sort' => 9,
                        'link' => true,
                    ],
                ],
                'type' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'type',
                        'sort' => 10,
                    ],
                ],
                'attribute_download' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_download',
                        'sort' => 11,
                    ],
                ],
                'attribute_href' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_href',
                        'sort' => 12,
                    ],
                ],
                'attribute_hreflang' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_hreflang',
                        'sort' => 13,
                    ],
                ],
                'attribute_media' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_media',
                        'sort' => 14,
                        'link' => true,
                    ],
                ],
                'attribute_ping' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_ping',
                        'sort' => 15,
                    ],
                ],
                'attribute_rel' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_rel',
                        'sort' => 16,
                    ],
                ],
                'attribute_target' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_target',
                        'sort' => 17,
                    ],
                ],
                'attribute_type' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_type',
                        'sort' => 18,
                    ],
                ],
                'attribute_class' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_class',
                        'sort' => 19,
                    ],
                ],
                'attribute_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_id',
                        'sort' => 20,
                    ],
                ],
                'attribute_style' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_style',
                        'sort' => 21,
                    ],
                ],
                'attribute_tabindex' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_tabindex',
                        'sort' => 22,
                    ],
                ],
                'attribute_title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attribute_title',
                        'sort' => 23,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-display_start' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                'field-display_end' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                                'field-menu_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'menu_id',
                                                    ],
                                                ],
                                                'field-parent_menuitem_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'parent_menuitem_id',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-position' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'position',
                                                    ],
                                                ],
                                                'field-contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'contentmeta_id',
                                                    ],
                                                ],
                                                'field-type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'type',
                                                    ],
                                                ],
                                                'field-attribute_download' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_download',
                                                    ],
                                                ],
                                                'field-attribute_href' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_href',
                                                    ],
                                                ],
                                                'field-attribute_hreflang' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_hreflang',
                                                    ],
                                                ],
                                                'field-attribute_media' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_media',
                                                    ],
                                                ],
                                                'field-attribute_ping' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_ping',
                                                    ],
                                                ],
                                                'field-attribute_rel' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_rel',
                                                    ],
                                                ],
                                                'field-attribute_target' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_target',
                                                    ],
                                                ],
                                                'field-attribute_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_type',
                                                    ],
                                                ],
                                                'field-attribute_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_class',
                                                    ],
                                                ],
                                                'field-attribute_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_id',
                                                    ],
                                                ],
                                                'field-attribute_style' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_style',
                                                    ],
                                                ],
                                                'field-attribute_tabindex' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_tabindex',
                                                    ],
                                                ],
                                                'field-attribute_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attribute_title',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%menuitem}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'date-display_start' => ['display_start', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_start', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-display_end' => ['display_end', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_end', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'required-menu_id' => ['menu_id', 'required'],
            'required-name' => ['name', 'required'],
            'integer-menu_id' => ['menu_id', 'integer'],
            'integer-parent_menuitem_id' => ['parent_menuitem_id', 'integer'],
            'integer-position' => ['position', 'integer'],
            'integer-contentmeta_id' => ['contentmeta_id', 'integer'],
            'integer-attribute_tabindex' => ['attribute_tabindex', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-name' => ['name', 'string', 'max' => 255],
            'string-type' => ['type', 'string', 'max' => 255],
            'string-attribute_download' => ['attribute_download', 'string', 'max' => 255],
            'string-attribute_href' => ['attribute_href', 'string', 'max' => 255],
            'string-attribute_hreflang' => ['attribute_hreflang', 'string', 'max' => 255],
            'string-attribute_media' => ['attribute_media', 'string', 'max' => 255],
            'string-attribute_ping' => ['attribute_ping', 'string', 'max' => 255],
            'string-attribute_rel' => ['attribute_rel', 'string', 'max' => 255],
            'string-attribute_target' => ['attribute_target', 'string', 'max' => 255],
            'string-attribute_type' => ['attribute_type', 'string', 'max' => 255],
            'string-attribute_class' => ['attribute_class', 'string', 'max' => 255],
            'string-attribute_id' => ['attribute_id', 'string', 'max' => 255],
            'string-attribute_style' => ['attribute_style', 'string', 'max' => 255],
            'string-attribute_title' => ['attribute_title', 'string', 'max' => 255],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-contentmeta_id' => [['contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['contentmeta_id' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-menu_id' => [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::class, 'targetAttribute' => ['menu_id' => 'id']],
            'exist-parent_menuitem_id' => [['parent_menuitem_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_menuitem_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-site', 'ID'),
            'status' => Yii::t('fafcms-site', 'Status'),
            'display_start' => Yii::t('fafcms-site', 'Display Start'),
            'display_end' => Yii::t('fafcms-site', 'Display End'),
            'menu_id' => Yii::t('fafcms-site', 'Menu ID'),
            'parent_menuitem_id' => Yii::t('fafcms-site', 'Parent Menuitem ID'),
            'name' => Yii::t('fafcms-site', 'Name'),
            'position' => Yii::t('fafcms-site', 'Position'),
            'contentmeta_id' => Yii::t('fafcms-site', 'Contentmeta ID'),
            'type' => Yii::t('fafcms-site', 'Type'),
            'attribute_download' => Yii::t('fafcms-site', 'Attribute Download'),
            'attribute_href' => Yii::t('fafcms-site', 'Attribute Href'),
            'attribute_hreflang' => Yii::t('fafcms-site', 'Attribute Hreflang'),
            'attribute_media' => Yii::t('fafcms-site', 'Attribute Media'),
            'attribute_ping' => Yii::t('fafcms-site', 'Attribute Ping'),
            'attribute_rel' => Yii::t('fafcms-site', 'Attribute Rel'),
            'attribute_target' => Yii::t('fafcms-site', 'Attribute Target'),
            'attribute_type' => Yii::t('fafcms-site', 'Attribute Type'),
            'attribute_class' => Yii::t('fafcms-site', 'Attribute Class'),
            'attribute_id' => Yii::t('fafcms-site', 'Attribute ID'),
            'attribute_style' => Yii::t('fafcms-site', 'Attribute Style'),
            'attribute_tabindex' => Yii::t('fafcms-site', 'Attribute Tabindex'),
            'attribute_title' => Yii::t('fafcms-site', 'Attribute Title'),
            'created_by' => Yii::t('fafcms-site', 'Created By'),
            'updated_by' => Yii::t('fafcms-site', 'Updated By'),
            'activated_by' => Yii::t('fafcms-site', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-site', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-site', 'Deleted By'),
            'created_at' => Yii::t('fafcms-site', 'Created At'),
            'updated_at' => Yii::t('fafcms-site', 'Updated At'),
            'activated_at' => Yii::t('fafcms-site', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-site', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-site', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[Contentmeta]].
     *
     * @return ActiveQuery
     */
    public function getContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[Menu]].
     *
     * @return ActiveQuery
     */
    public function getMenu(): ActiveQuery
    {
        return $this->hasOne(Menu::class, [
            'id' => 'menu_id',
        ]);
    }

    /**
     * Gets query for [[ParentMenuitem]].
     *
     * @return ActiveQuery
     */
    public function getParentMenuitem(): ActiveQuery
    {
        return $this->hasOne(static::class, [
            'id' => 'parent_menuitem_id',
        ]);
    }

    /**
     * Gets query for [[ParentMenuitemMenuitems]].
     *
     * @return ActiveQuery
     */
    public function getParentMenuitemMenuitems(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'parent_menuitem_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
