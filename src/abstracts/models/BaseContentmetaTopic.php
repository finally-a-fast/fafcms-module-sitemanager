<?php

namespace fafcms\sitemanager\abstracts\models;

use fafcms\fafcms\{
    inputs\NumberInput,
    inputs\ExtendedDropDownList,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\sitemanager\{
    models\Contentmeta,
    models\Topic,
};
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the abstract model class for table "{{%contentmeta_topic}}".
 *
 * @package fafcms\sitemanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $contentmeta_id
 * @property int $topic_id
 *
 * @property Contentmeta $contentmeta
 * @property Topic $topic
 */
abstract class BaseContentmetaTopic extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'contentmetatopic';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'contentmetatopic';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-site', 'ContentmetaTopics');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-site', 'ContentmetaTopic');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'topic_id' => static function(...$params) {
                return Topic::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'topic_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('topic_id', false),
                'relationClassName' => Topic::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'contentmeta_id',
                        'sort' => 2,
                        'link' => true,
                    ],
                ],
                'topic_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'topic_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'contentmeta_id',
                                                    ],
                                                ],
                                                'field-topic_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'topic_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%contentmeta_topic}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-contentmeta_id' => ['contentmeta_id', 'required'],
            'required-topic_id' => ['topic_id', 'required'],
            'integer-contentmeta_id' => ['contentmeta_id', 'integer'],
            'integer-topic_id' => ['topic_id', 'integer'],
            'exist-contentmeta_id' => [['contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['contentmeta_id' => 'id']],
            'exist-topic_id' => [['topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Topic::class, 'targetAttribute' => ['topic_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-site', 'ID'),
            'contentmeta_id' => Yii::t('fafcms-site', 'Contentmeta ID'),
            'topic_id' => Yii::t('fafcms-site', 'Topic ID'),
        ]);
    }

    /**
     * Gets query for [[Contentmeta]].
     *
     * @return ActiveQuery
     */
    public function getContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[Topic]].
     *
     * @return ActiveQuery
     */
    public function getTopic(): ActiveQuery
    {
        return $this->hasOne(Topic::class, [
            'id' => 'topic_id',
        ]);
    }
}
