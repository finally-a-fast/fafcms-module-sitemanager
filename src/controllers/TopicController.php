<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-sitemanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-sitemanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-sitemanager/docs Documentation of fafcms-module-sitemanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\sitemanager\controllers;

use fafcms\sitemanager\models\Topic;
use fafcms\helpers\DefaultController;

/**
 * Class TopicController
 * @package fafcms\sitemanager\controllers
 */
class TopicController extends DefaultController
{
    public static $modelClass = Topic::class;
}
