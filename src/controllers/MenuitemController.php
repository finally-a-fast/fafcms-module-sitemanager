<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */


namespace fafcms\sitemanager\controllers;

use fafcms\helpers\DefaultController;
use fafcms\sitemanager\models\Menuitem;


/**
 * MenuitemController
 */
class MenuitemController extends DefaultController
{
    public static $modelClass = Menuitem::class;
}
