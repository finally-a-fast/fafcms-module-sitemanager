[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module site manager
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Return type in scenario functions of search models @StefanBrandenburger

### Changed
- Changed sitemap priority length to generate it as number field and not as boolean @cmoeke   
- Regenerated contentmeta model @cmoeke
- Moved ContentmetaTrait from helpers to this module @cmoeke
- Added return types to attributeLabels, rules and prefixableTableName @cmoeke
- Regenerated models @cmoeke
- Changed parser data for the new fafte parser @cmoeke
- Removed parent_site_id from site model @cmoeke
- Column sizes for fomantic @cmoeke
- Changed menu items @cmoeke

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- Basic doc folder structure @cmoeke fafcms-core#37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core#38
- $ignoreType to getIdByUrl, to get non content contentmeta elements @cmoeke
- Contentmeta to parser data @cmoeke
- Added cache to layout and site model @cmoeke
- Added html minify to layout, site and snippet model @cmoeke
- Added open graph data @cmoeke

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- Optimized url generation of contentmeta records by using the protocol which is defined in the project @cmoeke
- Changed allowed type of snippets
- Moved replacements initialisation from bootstrapWebApp to bootstrapApp for console compatibility @StefanBrandenburger
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke
- Replaced dosamigos\tinymce widget with own widget @cmoeke
- Improved menu parser data @cmoeke
- Regenerated contentmeta and contentmetatopic models @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46

###Removed
- Useless SitemanagerAsset.php @cmoeke
- Removed useless query in layout model @cmoeke

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-sitemanager/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-sitemanager/-/tree/v0.1.0-alpha
